﻿using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

namespace DefaultNamespace
{
    public abstract class Shot : MonoBehaviour
    {
        [SerializeField] private float _reload;
        [SerializeField] private float _bulletSpeed;
    
        private BulletFactory _bulletFactory;
        
        protected bool _canFire = true;
    
        protected virtual void Start()
        {
            _bulletFactory = GetComponentInChildren<BulletFactory>();
        }
    
        protected void Shoot()
        {
            Transform bullet = MakeABullet();
    
            if (!bullet)
            {
                return;
            }
    
            AddForceToBullet(bullet);
        }
    
        private Transform MakeABullet()
        {
            StartCoroutine(StartReload());
            
            if (!_bulletFactory)
            {
                return null;
            }
    
            Transform bullet = _bulletFactory.GetInstance();
    
            BulletDetails bulletDetails = bullet.AddComponent<BulletDetails>();
            bulletDetails.SetOwner(transform);
    
            return bullet;
        }
    
        private void AddForceToBullet(Transform bullet)
        {
            Rigidbody2D rigidbody = bullet.GetComponent<Rigidbody2D>();
    
            rigidbody.velocity = transform.up * _bulletSpeed;
        }
    
        private IEnumerator StartReload()
        {
            _canFire = false;
    
            yield return new WaitForSeconds(_reload);
    
            _canFire = true;
        }
    }
}