using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class AIMover : Mover
{
    [SerializeField] private float _evadeRadius;

    [SerializeField] private float _borderXTop;
    [SerializeField] private float _borderXBot;
    [SerializeField] private float _borderYTop;
    [SerializeField] private float _borderYBot;

    [SerializeField] private int _neighborDistance;

    private Transform _playerTransform;

    private bool _stateIsRuning = false;
    private Coroutine _stateCoroutine;

    protected override void Start()
    {
        base.Start();

        _playerTransform = FindObjectOfType<PlayerMover>().transform;
        
        StartCoroutine(SelectTypeOfMoveAndMove());
    }

    private IEnumerator SelectTypeOfMoveAndMove()
    {
        StartCoroutine(EvadeBullet());
        
        while (true)
        {
            int state = Random.Range(0, 3);
            
            StopMove();
            
            switch (state)
            {
                case 0:
                    Debug.Log("random move");
                    _stateCoroutine = StartCoroutine(Move(SelectMoveVector()));
                    break;
                case 1:
                    Debug.Log("on player vision move");
                    _stateCoroutine = StartCoroutine(Move(SelectPositionOnPlayerVision()));
                    break;
                case 2:
                    Debug.Log("in hideout move");
                    _stateCoroutine = StartCoroutine(Move(SelectPositionInHideout()));
                    break;
            }

            yield return new WaitUntil(() => !_stateIsRuning);
        }
    }

    private Vector2 SelectMoveVector()
    {
        _inputX = Random.Range(-1f, 1f);
        _inputY = Random.Range(-1f, 1f);

        Vector2 moveVector = (Vector2)transform.position + new Vector2(_inputX, _inputY);

        if (Physics2D.OverlapPoint(moveVector))
        {
            SelectMoveVector();
        }

        return moveVector;
    }
    
    private IEnumerator EvadeBullet()
    {
        while(true)
        {
            
            RaycastHit2D[] allColliders = Physics2D.CircleCastAll(transform.position, _evadeRadius, Vector2.zero);

            allColliders = allColliders.Where(_ =>
            {
                bool b = false;
                
                if (_.collider.gameObject.TryGetComponent(out BulletDetails bullet))
                {
                    if (bullet.Owner != transform)
                    {
                        b = true;
                    }
                }

                return b;
            }).ToArray();
            
            foreach (RaycastHit2D i in allColliders)
            {
                Rigidbody2D rb = i.collider.gameObject.GetComponent<Rigidbody2D>();

                RaycastHit2D[] hit = Physics2D.RaycastAll(rb.position, rb.velocity, _evadeRadius);

                Collider2D playerHit = hit.FirstOrDefault(_ => _.collider.gameObject == gameObject).collider;
                
                if (playerHit)
                {
                    Debug.Log("evade move");
                    
                    if (_stateCoroutine != null)
                    {
                        StopCoroutine(_stateCoroutine);
                    }

                    bool rand = Convert.ToBoolean(Random.Range(0, 2));
                    
                    StopMove();
                    
                    if (rand)
                    {
                        StartCoroutine(Move(transform.position + playerHit.gameObject.transform.right));
                    }
                    else
                    {
                        StartCoroutine(Move(transform.position - playerHit.gameObject.transform.right));
                    }
                }
            }

            yield return new WaitForSeconds(1);
        }
    }

    private IEnumerator Move(Vector2 position)
    {
        _stateIsRuning = true;
        
        List<Vector2> moveMap = GetMoveMap(position);

        if (moveMap == null)
        {
            Debug.LogError("Path not found!");
            yield break;
        }

        foreach (Vector2 i in moveMap)
        {
            Vector2 direction = (i - (Vector2)transform.position).normalized;
            _inputX = direction.x;
            _inputY = direction.y;

            Move();
            
            float time = i.magnitude / (_speed*Time.deltaTime);

            yield return new WaitForSeconds(time);
        }

        _stateIsRuning = false;
    }

    private Vector2 SelectPositionOnPlayerVision()
    {
        float dotY = Random.Range(_borderYBot, _borderYTop);
        float dotX = Random.Range(_borderXBot, _borderXTop);
        Vector2 selectedPosition = new Vector2(dotX, dotY);
        
        Vector2 direction = selectedPosition - (Vector2)_playerTransform.position;

        LayerMask mask = ~(1 << 6);
        
        if (Physics2D.OverlapPoint(selectedPosition) 
            || Physics2D.Raycast(_playerTransform.position, direction, Vector2.Distance(selectedPosition, _playerTransform.position), mask).collider)
        {
            return SelectPositionOnPlayerVision();
        }

        return selectedPosition;
    }
    private Vector2 SelectPositionInHideout()
    {
        float dotY = Random.Range(_borderYBot, _borderYTop);
        float dotX = Random.Range(_borderXBot, _borderXTop);
        Vector2 selectedPosition = new Vector2(dotX, dotY);
        
        Vector2 direction = selectedPosition - (Vector2)_playerTransform.position;

        LayerMask mask = ~(1 << 6);
        
        if (Physics2D.OverlapPoint(selectedPosition) 
            || !Physics2D.Raycast(_playerTransform.position, direction, Vector2.Distance(selectedPosition, _playerTransform.position), mask).collider)
        {
            return SelectPositionOnPlayerVision();
        }

        return selectedPosition;
    }

    private List<Vector2> GetMoveMap(Vector2 endPosition)
    {
        return FindPath((Vector2)transform.position, endPosition);
    }

    private List<Vector2> FindPath(Vector2 start, Vector2 end)
    {
        Dictionary<Vector2, Node> nodes = new Dictionary<Vector2, Node>();
        List<Node> openList = new List<Node>();
        HashSet<Node> closedList = new HashSet<Node>();

        Node startNode = new Node(start);
        Node endNode = new Node(end);

        nodes[start] = startNode;
        nodes[end] = endNode;

        startNode.GCost = 0;
        startNode.HCost = Vector2.Distance(start, end);
        openList.Add(startNode);

        while (openList.Count > 0)
        {
            Node currentNode = openList.OrderBy(node => node.FCost).First();
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (Vector2.Distance(currentNode.Position, end) < _neighborDistance)
            {
                return RetracePath(startNode, currentNode);
            }

            foreach (Vector2 neighborPosition in GetNeighbors(currentNode.Position))
            {
                if (!nodes.ContainsKey(neighborPosition))
                {
                    nodes[neighborPosition] = new Node(neighborPosition);
                }

                Node neighbor = nodes[neighborPosition];

                if (closedList.Contains(neighbor))
                {
                    continue;
                }

                float newGCost = currentNode.GCost + Vector2.Distance(currentNode.Position, neighbor.Position);

                if (newGCost < neighbor.GCost || !openList.Contains(neighbor))
                {
                    neighbor.GCost = newGCost;
                    neighbor.HCost = Vector2.Distance(neighbor.Position, end);
                    neighbor.Parent = currentNode;

                    if (!openList.Contains(neighbor))
                    {
                        openList.Add(neighbor);
                    }
                }
            }
        }

        return null;
    }

    private List<Vector2> RetracePath(Node startNode, Node endNode)
    {
        List<Vector2> path = new List<Vector2>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode.Position);
            currentNode = currentNode.Parent;
        }

        path.Reverse();
        return path;
    }

    private List<Vector2> GetNeighbors(Vector2 position)
    {
        List<Vector2> neighbors = new List<Vector2>
        {
            new Vector2(position.x + _neighborDistance, position.y),
            new Vector2(position.x - _neighborDistance, position.y),
            new Vector2(position.x, position.y + _neighborDistance),
            new Vector2(position.x, position.y - _neighborDistance),
            new Vector2(position.x + _neighborDistance, position.y + _neighborDistance),
            new Vector2(position.x - _neighborDistance, position.y - _neighborDistance),
            new Vector2(position.x + _neighborDistance, position.y - _neighborDistance),
            new Vector2(position.x - _neighborDistance, position.y + _neighborDistance)
        };
        
        List<Vector2> validNeighbors = new List<Vector2>();
        foreach (var neighbor in neighbors)
        {
            RaycastHit2D hit = Physics2D.Raycast(position, neighbor - position, _neighborDistance, 6);
            if (!hit.collider)
            {
                validNeighbors.Add(neighbor);
            }
            else
            {
                Debug.Log($"Obstacle detected from {position} to {neighbor} at distance {_neighborDistance}");
            }
        }

        return validNeighbors;
    }
    private class Node
    {
        public Vector2 Position;
        public float GCost;
        public float HCost;
        public Node Parent;

        public float FCost => GCost + HCost;

        public Node(Vector2 position)
        {
            Position = position;
            GCost = float.MaxValue;
            HCost = 0;
            Parent = null;
        }
    }   
    
}
