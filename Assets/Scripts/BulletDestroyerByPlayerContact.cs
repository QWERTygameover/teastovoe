using UnityEngine;

public class BulletDestroyerByPlayerContact : BulletDestroyer
{
    private Collision2D _lastCollision;
    
    protected override bool CheckCondition()
    {
        return _lastCollision.gameObject.CompareTag("Player");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _lastCollision = other;
        
        Destroy();
    }
}
