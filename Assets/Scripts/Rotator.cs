using UnityEngine;

public abstract class Rotator : MonoBehaviour
{
    [SerializeField] private float _speed;

    protected float _inputX;

    protected virtual void Rotate()
    {
        Vector3 rotationVector = new Vector3(0, 0, _speed * _inputX) * Time.deltaTime;
        
        transform.Rotate(rotationVector);
    }
}
