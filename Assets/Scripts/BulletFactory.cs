using UnityEngine;

public class BulletFactory : MonoBehaviour
{
    [SerializeField] private Transform _prefab;
    [SerializeField] private Transform _instantiatePoint;

    public Transform GetInstance()
    {
        Transform bullet = Instantiate(_prefab, _instantiatePoint.position, transform.rotation);

        return bullet;
    }
}
