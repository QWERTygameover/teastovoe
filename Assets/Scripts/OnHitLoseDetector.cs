using UnityEngine;

public class OnHitLoseDetector : LoseDetector
{
    private Transform _lastCollision;
    
    protected override bool CheckOnLoseConditions()
    {
        if (!_lastCollision.TryGetComponent(out BulletDetails bullet))
        {
            return false;
        }

        if (bullet.Owner == gameObject.transform)
        {
            return false;
        }

        return true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _lastCollision = other.transform;
        
        CheckLose();
    }
}
