using UnityEngine;

public class PlayerRotator : Rotator
{
    private void FixedUpdate()
    {
        Rotate();
    }

    protected override void Rotate()
    {
        _inputX = Input.GetAxis("HorizontalRotate");
        
        base.Rotate();
    }
}
