using UnityEngine;
public class Ricochet : MonoBehaviour
{
    private Rigidbody2D _rigidbody;

    private Vector2 _velocity;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();

        _velocity = _rigidbody.velocity;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Bounce(other);
    }

    private void Bounce(Collision2D other)
    {
        _velocity = CalculateVector(other);
        
        _rigidbody.velocity = _velocity;
    }

    private Vector2 CalculateVector(Collision2D other)
    {
        Vector2 bounceVector = new Vector2();
        
        foreach (ContactPoint2D contact in other.contacts)
        {
            float dot = Vector2.Dot(_velocity, contact.normal);
            
            bounceVector = _velocity - 2 * dot * contact.normal;
        }

        return bounceVector;
    }
}
