using UnityEngine;

public abstract class BulletDestroyer : MonoBehaviour
{
    protected abstract bool CheckCondition();

    protected void Destroy()
    {
        if (!CheckCondition())
        {
            return;
        }
        
        Destroy(gameObject);
    }
}
