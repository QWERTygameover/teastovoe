using UnityEngine;

public class BulletDetails : MonoBehaviour
{
    private Transform _owner;

    public Transform Owner => _owner;
    public void SetOwner(Transform owner) => _owner = owner;
}
