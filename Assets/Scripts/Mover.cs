using UnityEngine;

public abstract class Mover : MonoBehaviour
{
    [SerializeField] protected float _speed;

    protected float _inputX;
    protected float _inputY;

    private Rigidbody2D _rigidbody;
    
    protected virtual void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    protected virtual void Move()
    {
        Vector2 moveVector = CalculateVelocity(_inputX, _inputY);

        _rigidbody.velocity = moveVector;
    }

    private Vector2 CalculateVelocity(float inputX, float inputY)
    {
        return new Vector2(inputX, inputY) * (_speed * Time.deltaTime);
    }

    protected void StopMove()
    {
        _rigidbody.velocity = Vector2.zero;
    }
}
