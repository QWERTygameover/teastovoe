using DefaultNamespace;
using UnityEngine;

public class PlayerShot : Shot
{
    protected  void Update()
    {
        if (!_canFire)
        {
            return;
        }
        
        if (Input.GetAxis("Fire1") > 0)
        {
            Shoot();
        }
    }
}
