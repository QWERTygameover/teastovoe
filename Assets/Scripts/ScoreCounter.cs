using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    public event Action<List<PlayerScore>> OnScoreChanged;
    
    private LoseDetector[] _players;

    private List<PlayerScore> _scores = new List<PlayerScore>();

    private void Awake()
    {
        _players = FindObjectsOfType<LoseDetector>();

        foreach (LoseDetector player in _players)
        {
            PlayerScore playerScore = new PlayerScore(player);
            
            _scores.Add(playerScore);
        }
    }

    public void IncreaseScore(LoseDetector looser)
    {
        PlayerScore winner = _scores.FirstOrDefault(_ => _.Player != looser);

        winner.IncreaseScore();
        
        OnScoreChanged?.Invoke(_scores);
    }
}

public class PlayerScore
{
    public LoseDetector Player { get; }
    
    private int _score;

    public PlayerScore(LoseDetector player)
    {
        Player = player;
        _score = 0;
    }

    public int Score => _score;
    
    public void IncreaseScore() => _score+=1;
}