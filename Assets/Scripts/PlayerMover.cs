using UnityEngine;

public class PlayerMover : Mover
{
    private void FixedUpdate()
    {
        Move();
    }

    protected override void Move()
    {
        _inputX = Input.GetAxis("Horizontal");
        _inputY = Input.GetAxis("Vertical");

        base.Move();
    }
}
