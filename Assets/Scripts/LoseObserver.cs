using UnityEngine;

public class LoseObserver : MonoBehaviour
{
    private LoseDetector[] _detectors;

    private ScoreCounter _scoreCounter;
    
    private void Awake()
    {
        _detectors = FindObjectsOfType<LoseDetector>();
        _scoreCounter = FindObjectOfType<ScoreCounter>();
        
        SubscribeToAllDetectors();
    }

    private void SubscribeToAllDetectors()
    {
        foreach (LoseDetector detector in _detectors)
        {
            detector.OnLose += _scoreCounter.IncreaseScore;
        }   
    }
}
