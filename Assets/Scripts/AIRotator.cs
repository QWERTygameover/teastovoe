using System.Collections;
using UnityEngine;

public class AIRotator : Rotator
{
    [SerializeField] private float _timeBetweenRotations;
    
    private Rigidbody2D _player;
    
    private void Start()
    {
        _player = FindObjectOfType<PlayerRotator>().GetComponent<Rigidbody2D>();
        
        StartCoroutine(RotateNavigation());
    }

    private IEnumerator RotateNavigation()
    {
        while (true)
        {

            Rotate();
            
            yield return new WaitForSeconds(_timeBetweenRotations);
        }  
    }
    
    protected override void Rotate()
    {
        transform.up = _player.position - (Vector2)transform.position;
    }
}
