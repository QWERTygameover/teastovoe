using System;
using UnityEngine;

public abstract class LoseDetector : MonoBehaviour
{
    public event Action<LoseDetector> OnLose;
    
    protected void CheckLose()
    {
        if (!CheckOnLoseConditions())
        {
            return;
        }
        
        OnLose?.Invoke(this); 
    }
    
    protected abstract bool CheckOnLoseConditions();
}
