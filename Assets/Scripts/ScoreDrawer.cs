using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreDrawer : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;

    [SerializeField] private ScoreCounter _scoreCounter;

    private void OnEnable()
    {
        _scoreCounter.OnScoreChanged += RedrawScore;
    }

    private void OnDisable()
    {
        _scoreCounter.OnScoreChanged -= RedrawScore;
    }

    private void RedrawScore(List<PlayerScore> scores)
    {
        string text = "";
        
        foreach (PlayerScore i in scores)
        {
            text += $"{i.Player.gameObject.name,10} ";
        }

        text += '\n';
        
        foreach (PlayerScore i in scores)
        {
            text += $"{i.Score.ToString(),10} ";
        }

        _scoreText.text = text;
    }
}
