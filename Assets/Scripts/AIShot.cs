using System;
using DefaultNamespace;
using UnityEngine;

public class AIShot : Shot
{
    private Transform _player;

    protected override void Start()
    {
        base.Start();

        _player = FindObjectOfType<PlayerShot>().transform;
    }

    protected void Update()
    {
        if (!_canFire)
        {
            return;
        }
        
        FindPlayerAndShoot();
    }

    private void FindPlayerAndShoot()
    {
        LayerMask mask = ~(1 << 6);
        
        Collider2D hit = Physics2D.Raycast(transform.position, _player.position-transform.position,
            Vector2.Distance(transform.position, _player.position), mask).collider;
        
        
        Debug.DrawRay(transform.position, _player.position-transform.position, Color.red, 1f);
        
        if (hit == null)
        {
            Shoot();
        }
    }
}
