using UnityEngine;

public class BulletDestroyerByBorder : BulletDestroyer
{
    private Collision2D _lastCollision;
    
    protected override bool CheckCondition()
    {
        return _lastCollision.gameObject.CompareTag("Border");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _lastCollision = other;
        
        Destroy();
    }
}
